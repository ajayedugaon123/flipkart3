package com.example.flipkart3

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class ProductAdapterMain(private val array: ArrayList<MovieModel>, private val context: Context) : RecyclerView.Adapter<ProductAdapterMain.ItemView>(){

    class ItemView(view: View): RecyclerView.ViewHolder(view){
        val title = view.findViewById<TextView>(R.id.title)
        val recyle=view.findViewById<RecyclerView>(R.id.recylerview)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView {
        val ItemView=
            LayoutInflater.from(parent.context).inflate(R.layout.recycle_view_item,parent,false)
        return ItemView(ItemView)

    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        holder.title.text = array[position].title
        if (array[position].type == 1){
            holder.recyle.layoutManager  = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,true)
            holder.recyle.adapter = secondadapter(array[position].products)
        }
        else if (array[position].type == 2){
            holder.recyle.layoutManager  = GridLayoutManager(context,2)
            holder.recyle.adapter = secondadapter(array[position].products)
        }
        else{
            holder.recyle.layoutManager  = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,true)
            holder.recyle.adapter = secondadapter(array[position].products)
        }
    }


}