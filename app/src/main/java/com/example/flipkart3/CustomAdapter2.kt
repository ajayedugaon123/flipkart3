package com.example.flipkart3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class secondadapter(private val array: ArrayList<Product>) : RecyclerView.Adapter<secondadapter.ItemView>(){

    class ItemView(view: View): RecyclerView.ViewHolder(view){

        val title = view.findViewById<TextView>(R.id.productname)
        val price = view.findViewById<TextView>(R.id.productprice)
        val recyle=view.findViewById<ImageView>(R.id.img)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemView {
        return ItemView(LayoutInflater.from(parent.context).inflate(R.layout.recycle_view_data,parent,false))
    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: ItemView, position: Int) {
        holder.title.text = array[position].name
        holder.price.text = array[position].price.toString()
        holder.recyle.setImageResource(array[position].imageUrl)
    }

}