package com.example.flipkart3

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
                val recycler=findViewById<RecyclerView>(R.id.recycleview)
                recycler.layoutManager = LinearLayoutManager(this)
                val listview=ArrayList<MovieModel>()
                val array = ArrayList<Product>()

                array.add(Product("Burger",38999,R.drawable.burger))
                array.add(Product("Notebook",35999,R.drawable.notebook))
                array.add(Product("Dahibara",35999,R.drawable.dahibara))
                array.add(Product("Bycycle",35999,R.drawable.bycycle))
                array.add(Product("Apple",35999,R.drawable.apple))
                array.add(Product("Shoes",35999,R.drawable.shoes))
                array.add(Product("Ajay",35999,R.drawable.ajay))

                array.add(Product("Hydrabadi Biryani",35999,R.drawable.hydrabadibiryani))

                listview.add(MovieModel("Status",1,array))
                listview.add(MovieModel("game",2,array))
                listview.add(MovieModel("game",3,array))
                listview.add(MovieModel("game",3,array))
                listview.add(MovieModel("game",3,array))
                listview.add(MovieModel("game",3,array))
                listview.add(MovieModel("game",3,array))
                listview.add(MovieModel("game",3,array))
                listview.add(MovieModel("game",3,array))

                recycler.layoutManager = LinearLayoutManager(this,LinearLayoutManager.VERTICAL,true)

                recycler.adapter = ProductAdapterMain(listview,this)
            }
        }

