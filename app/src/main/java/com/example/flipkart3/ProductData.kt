package com.example.flipkart3

data class MovieModel(
    val title: String,
    val type: Int,
    val products : ArrayList<Product>
)
data class Product(
    val name: String,
    val price : Int,
    val imageUrl: Int
)
